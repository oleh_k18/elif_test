<?php


class Shops
{
    /**
     * return order_id
     */
    public static function saveOrder($user_id, $total_price)
    {
        $id = 0;
        $isHuman = GoogleCaptcha::isHuman();

        if ($isHuman) {
            $db = Db::getConnection();
            $q = "INSERT INTO orders (`user_id`, total_price)
                VALUES (:user_id, :total_price)";
            $result = $db->prepare($q);
            $result->bindParam(':user_id', $user_id, PDO::PARAM_INT);
            $result->bindParam(':total_price', $total_price, PDO::PARAM_INT);
            $result->execute();
            $id = $db->lastInsertId();
        }
        
        return $id;
        
    }

    /**
     * return order_id
     */
    public static function saveShopingCart($user_id, $order_id, $shop_id, $good_id, $count)
    {
        $id = 0;
        $isHuman = GoogleCaptcha::isHuman();

        if ($isHuman) {
            $db = Db::getConnection();
            $q = "INSERT INTO shopingCarts (`user_id`, order_id, shop_id, good_id, `count`)
                VALUES (:user_id, :order_id, :shop_id, :good_id, :count)";
            $result = $db->prepare($q);
            $result->bindParam(':user_id', $user_id, PDO::PARAM_INT);
            $result->bindParam(':order_id', $order_id, PDO::PARAM_INT);
            $result->bindParam(':shop_id', $shop_id, PDO::PARAM_INT);
            $result->bindParam(':good_id', $good_id, PDO::PARAM_INT);
            $result->bindParam(':count', $count, PDO::PARAM_INT);
            $result->execute();
            $id = $db->lastInsertId();
        }

        return $id;
        
    }

    public static function getShopsList()
    {
        $shopsList = array();
        $isHuman = GoogleCaptcha::isHuman();

        if ($isHuman) {
            $db = Db::getConnection();
            $q = "SELECT * FROM shops";
            $result = $db->query($q);
    
            $result->setFetchMode(PDO::FETCH_ASSOC);  
            
            while ($row = $result->fetchObject()) {
                $shopsList[] = $row;
            }
        }

        return $shopsList;

    }
}
