<?php


class History
{
    /**
     * return array of user orders
     */
    public static function getOrdersByUserId($user_id)
    {
        $ordersList = array();
        $isHuman = GoogleCaptcha::isHuman();

        if ($isHuman) {
            $db = Db::getConnection();
            $q = "SELECT * 
                    FROM `orders` 
                    WHERE user_id = :user_id";
            $result = $db->prepare($q);
            $result->bindParam(':user_id', $user_id, PDO::PARAM_INT);
            $result->execute();

            while ($row = $result->fetchObject()) {
                $ordersList[] = $row;
            }
        }

        return $ordersList;
        
    }

    /**
     * return array of order goods
     */
    public static function getGoodsByOrderId($order_id)
    {
        $goodsList = array();
        $isHuman = GoogleCaptcha::isHuman();

        if ($isHuman) {
            $db = Db::getConnection();
            $q = "SELECT 
                        g.id AS 'good_id',
                        g.name AS 'good_name',
                        s.count AS 'count',
                        g.price AS 'price',
                        g.icon AS 'icon'
                    FROM `shopingCarts` s
                        LEFT JOIN goods g
                            ON good_id = g.id
                    WHERE order_id = :order_id";
            $result = $db->prepare($q);
            $result->bindParam(':order_id', $order_id, PDO::PARAM_INT);
            $result->execute();

            while ($row = $result->fetchObject()) {
                $goodsList[] = $row;
            }
        }

        return $goodsList;
        
    }

}
