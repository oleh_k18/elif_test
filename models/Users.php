<?php


class Users
{
    /**
     * create user
     * return user_id
     */
    public static function createUser($name, $email, $phone, $address)
    {
        $id = 0;
        $isHuman = GoogleCaptcha::isHuman();
        
        if ($isHuman) {
            $db = Db::getConnection();
            $q = "INSERT INTO users (`name`, email, phone, `address`)
                VALUES (:name, :email, :phone, :address)";
            $result = $db->prepare($q);
            $result->bindParam(':name', $name, PDO::PARAM_STR, 64);
            $result->bindParam(':email', $email, PDO::PARAM_STR, 64);
            $result->bindParam(':phone', $phone, PDO::PARAM_STR, 16);
            $result->bindParam(':address', $address, PDO::PARAM_STR, 255);
            $result->execute();
            $id = $db->lastInsertId();
        }

        return $id;
        
    }

    /**
     * check, is user exist in system
     * return user_id
     */
    public static function getUserId($email, $phone)
    {
        $id = 0;
        $isHuman = GoogleCaptcha::isHuman();
        
        if ($isHuman) {
            $db = Db::getConnection();
            $q = "SELECT * 
                    FROM users
                    WHERE email = :email 
                        AND phone = :phone;";
            $result = $db->prepare($q);
            $result->bindParam(':email', $email, PDO::PARAM_STR, 64);
            $result->bindParam(':phone', $phone, PDO::PARAM_STR, 16);
            $result->execute();

            $row = $result->fetchObject();
            if (isset($row) && !empty($row) && isset($row->id) && $row->id > 0) {
                $id = $row->id;
            } else {
                $id = 0;
            }
        }
        
        return $id;
        
    }

}
