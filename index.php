<?php
header("Content-Type: text/html; charset=utf-8");
session_start();
// FRONT CONTROLLER

ini_set('display_errors', 1);
error_reporting(E_ALL);


define('ROOT', dirname(__FILE__));
require_once(ROOT.'/components/Autoload.php');



$router = new Router();
$router->run();

