
  var app = new Vue({
    el: "#app",
    data: {

        name: '',
        email: '',
        phone: '',
        address: '',

        input: 4,
        checked1: false,
        shops: [],
        shop: 0,
        shopShopingCart: 0,
        goods: [],
        shopingCart: [],

        shopClass: '',
        shopingCartClass: '',
        historyClass: 'primary',
        totalPrice: 0,
        
        oders: [],
        address_class: '',
        name_class: '',
        email_class: '',
        phone_class: '',

        captchaToken: '',
    },
    methods: {
        test (e='', t='', y='') {
            console.log(e,t,y);
        },
        changePage (page) {
            this.shopingCartClass = '';
            this.historyClass = '';
            this.shopClass = '';
            if (page == 'Shop') {
                this.shopClass = 'primary';
            } else if (page == 'history') {
                this.historyClass = 'primary';
            }else {
                this.shopingCartClass = 'primary';
                setTimeout(function () {
                    initMap();
                    autocomplete();
                }, 300);
            }
        },
        changeCountGoods (id, new_, old_) {
            if (new_ == 0) {
                var item2delete = this.shopingCart.find(e => e.id === id);
                var shop_id = item2delete.shop_id;
                var index = this.shopingCart.indexOf(item2delete);

                this.$message.success('Removed item from shoping cart');

                setTimeout(function () {app.shopingCart.splice(index, 1);
                
                    // check are we have goods from this shop
                    if (app.shopingCart.find(e => e.shop_id === shop_id) == undefined) {
                        app.shopShopingCart = 0;
                        app.shopClass = 'primary';
                        app.shopingCartClass = '';
                    }

                }, 100);

            }

            setTimeout(function () {localStorage.setItem('shopingCart', JSON.stringify(app.shopingCart));}, 200);
            this.calcTotalPrice();
        },
        add2Cart (id) {
            
            this.checkShopingCart();

            var selected = this.goods.find(e => e.id === id);
            if (this.shopingCart != null && this.shopingCart.find(e => e.id === id) != undefined){
                this.shopingCart.find(e => e.id === id).count++;
            } else {
                selected.count = 1;
                this.shopingCart.push(selected);
            }

            this.shopShopingCart = selected.shop_id;

            localStorage.setItem('shopingCart', JSON.stringify(this.shopingCart));
            this.calcTotalPrice();

            this.$message.success('Added to shoping cart');
        },
        getShops () {
            var data = '';
            var headers = {headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }};
            
            axios.post('/shops/list', data, headers)
            .then(response => {
                this.shops = response.data;
            });
        },
        getGoods () {
            var data = '';
            var headers = {headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }};
            
            axios.post('/goods/list', data, headers)
            .then(response => {
                console.log(response.data);
                this.goods = response.data;                

            });
        },
        saveShopingCart () {
            if (this.validateAddress() == false) return;
            if (this.validateEmail() == false) return;
            if (this.validateName() == false) return;
            if (this.validatePhone() == false) return;
            if (app.shopingCart.length == 0) {
                this.$message.error('Empty Shoping Cart');
                return;
            }

            var data = "name="+this.name +
                "&email="+this.email +
                "&phone="+this.phone +
                "&address="+this.address +
                "&totalPrice="+this.totalPrice +
                "&shop="+this.shopShopingCart +
                "&shopingCart="+encodeURIComponent(JSON.stringify(this.shopingCart));
            var headers = {headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }};
            
            axios.post('/shoping_cart/submit', data, headers)
            .then(response => {
                if (response.data.ok == 1) {
                    this.shopingCart.splice(0, this.shopingCart.length);
                    localStorage.removeItem('shopingCart');
                    this.shopShopingCart = 0;

                    this.$notify({ type: 'success', title: 'success', message: 'success'});

                    setTimeout(function () {
                        app.shopClass = 'primary';
                        app.shopingCartClass = '';
                    }, 1000);
                }

            });
        },
        getHistory () {

            if (this.validateEmail() == false) return;
            if (this.validatePhone() == false) return;
            
            var data = "email="+this.email +
            "&phone="+this.phone;
            var headers = {headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }};

            axios.post('/history/list', data, headers)
            .then(response => {
                if (response.data.length == 0) {
                   this.$notify({ type: 'error', title: 'Error', message: 'User didn\'t find'});
                }
                this.oders = response.data;

            });

        },

        /**
         * if already have selected item, but reloaded page
         * we will read selected item from localStorage
        */
        checkShopingCart () {
            if (localStorage.getItem('shopingCart') != null) { 
                var o = '';
                try {
                    o = JSON.parse(localStorage.getItem('shopingCart'));
                    this.shopingCart = o;
                    this.shopShopingCart = o[0].shop_id;
                } catch (error) {
                    console.log(error);
                }
            }
        },

        googleCaptcha () {
            
            var data = "token="+this.captchaToken;
            var headers = {headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }};

            axios.post('/google/index', data, headers)
            .then(response => {
                this.$notify({ type: 'info', title: 'info', message: response.data});

                this.getShops();
                this.getGoods();
                this.checkShopingCart();
                this.calcTotalPrice();

            });

        },
        calcTotalPrice () {
            this.totalPrice = 0;
            this.shopingCart.forEach(e => {
                this.totalPrice = parseInt(this.totalPrice) + parseInt(e.count) * parseInt(e.price);
            });
        },
        validateEmail() {
            if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(this.email)) {
                this.email_class = '';
            } else {
                this.$message.error('Wrong format Email');
                this.email_class = 'error';
                return false;
            }
        },
        validateAddress() {
            if (this.address != '') {
                this.address_class = '';
            } else {
                this.$message.error('Wrong format Address');
                this.address_class = 'error';
                return false;
            }
        },
        validateName() {
            if (/^([a-z,A-Z]{2,20})$/.test(this.name)) {
                this.name_class = '';
            } else {
                this.$message.error('Wrong format Name');
                this.name_class = 'error';
                return false;
            }
        },
        validatePhone() {
            if (/^([0-9]{12})$/.test(this.phone)) {
                this.phone_class = '';
            } else {
                this.$message.error('Wrong format Phone');
                this.phone_class = 'error';
                return false;
            }
        }
    },
    beforeMount() {
        grecaptcha.ready(function() {
            grecaptcha.execute('6LcKp7kgAAAAAMJY6z8a5AJ3lAvxbEsTOJES_PYe', {action: 'homepage'}).then(function(res) {
                console.log('captcha', res);
                app.captchaToken = res;
                app.googleCaptcha();
            });
        });
    }
  });