var searchInput = 'search_input';
 
var map;
var shopMarker;
var direcrionMarker;
var geocoder;

function initMap(lat = 50.4230480197085, lng = 30.51696801970851) {

    map = new google.maps.Map(document.getElementById("map"), {
        center: { lat: lat, lng: lng},
        zoom: 8,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    
    shopMarker = new google.maps.Marker({
        position: { lat: lat, lng: lng},
        map: map,
        fillColor: "blue",
        label: 'S',
        title: 'Shop',
    });

    geocoder = new google.maps.Geocoder();

    google.maps.event.addListener(map, 'click', function (event) {
        var lat, lng;
        lat = event.latLng.lat();
        lng = event.latLng.lng();

        if (direcrionMarker != undefined) {
            direcrionMarker.setPosition(event.latLng);

            codeLatLng(lat, lng);
            console.log('широта: '+lat+ "; довгота: " +lng);
        } else {
            setMarker (lat, lng);

            codeLatLng(lat, lng);
            console.log('широта: '+lat+ "; довгота: " +lng);
        }
    });
}

function setMarker (lat = 50.4230480197085, lng = 30.51696801970851, title = '') {

    if (direcrionMarker != undefined)
        direcrionMarker.setMap(null);
    
    direcrionMarker = new google.maps.Marker({
        position: { lat: lat, lng: lng},
        map: map,
        title: title
    });

    var latLng = new google.maps.LatLng(lat, lng);
    map.setCenter(latLng);
}

window.initMap = initMap;

function autocomplete () {

    var autocomplete;
    autocomplete = new google.maps.places.Autocomplete((document.getElementById(searchInput)), {
        types: ['geocode'],
    });

    google.maps.event.addListener(autocomplete, 'place_changed', function () {
        var near_place = autocomplete.getPlace();
        console.log(near_place);
        console.log(near_place.geometry.viewport);
        var lat = '';
        var lng = '';

        if (near_place.geometry.viewport.ub !== undefined) {
            lat = near_place.geometry.viewport.ub.lo;
        } else {
            if (near_place.geometry.viewport.wb !== undefined) {
                lat = near_place.geometry.viewport.wb.lo;
            }
        }


        if (near_place.geometry.viewport.Ra !== undefined) {
            lng = near_place.geometry.viewport.Ra.lo;
        } 

        console.log('широта: '+lat+ "; довгота: " +lng);
        setMarker(lat, lng);

    });


}


function codeLatLng(lat, lng) {
    var latlng = new google.maps.LatLng(lat, lng);
    geocoder.geocode({
      'latLng': latlng
    }, function (results, status) {
        if (status === google.maps.GeocoderStatus.OK) {
            if (results[1]) {
                var tmp = results[1];
                var address = tmp.formatted_address;
                app.address = address;
            } else {
                app.$message.error('No results found');
            }
        } else {
            app.$message.error('Geocoder failed due to: ' + status);
        }
    });
  }
  