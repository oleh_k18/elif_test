
<!-- shoping cart -->
<el-col :span="12" :xs="24" :sm="13" justify="center" type="flex" v-if="shopingCartClass == 'primary'">
    <div style="margin: auto;max-width: 1500px;">
    <el-col :xs="24"  :sm="24" :md="24" :lg="24" :xl="24" v-for="(row, index) in shopingCart" style="margin: 10px;display: flex">
        <el-card :body-style="{ padding: '0px', display: 'flex', height: '300px' }">
            <img  :src="row.icon" class="image-cart">
            <div style="padding: 14px">
                <div><b>{{row.name}}</b></div>
                <div class="bottom clearfix nowrap">
                    <span>
                        {{row.price}}.00 $
                    </span>
                    <br>
                    <br>
                    <el-input-number 
                        v-model="row.count" 
                        @change="(new_, old_) => changeCountGoods(row.id, new_, old_)" 
                        :min="0" 
                        :max="100"></el-input-number>
                </div>
            </div>
        </el-card>
    </el-col>
    </div>
</el-col>
