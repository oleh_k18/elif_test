
<!-- user form -->
<el-col :span="12" :xs="24" :sm="10" :offset="1"  class="nav user-form" v-if="shopingCartClass == 'primary'">
    <el-row justify="left" type="flex">
        <el-col :xs="24"  :sm="24" :md="24" :lg="24" :xl="24">
            <div id="map"></div>
        </el-col>
    </el-row>
    <el-row justify="center" type="flex">
        <el-col :xs="24"  :sm="24" :md="24" :lg="24" :xl="24">
            Address: <el-input id="search_input" 
                placeholder="Please input" 
                v-model="address" 
                @blur="validateAddress"  @focus="address_class = ''"
                :class="address_class"></el-input>
        </el-col>
    </el-row>
    <el-row justify="left" type="flex">
        <el-col :xs="24"  :sm="24" :md="24" :lg="24" :xl="24">
            Name: <el-input placeholder="Please input" 
                v-model="name" 
                @blur="validateName"  @focus="name_class = ''"
                :class="name_class"></el-input>
        </el-col>
    </el-row>
    <el-row justify="center" type="flex">
        <el-col :xs="24"  :sm="24" :md="24" :lg="24" :xl="24">
            Email: <el-input type="email" 
                placeholder="Please input" 
                v-model="email" 
                @blur="validateEmail" @focus="email_class = ''"
                :class="email_class"></el-input>
        </el-col>
    </el-row>
    <el-row justify="center" type="flex">
        <el-col :xs="24"  :sm="24" :md="24" :lg="24" :xl="24">
            Phone: <el-input type="number" 
                placeholder="380####" 
                v-model="phone" 
                @blur="validatePhone"  @focus="phone_class = ''"
                :class="phone_class"></el-input>
        </el-col>
    </el-row>
    
</el-col>
