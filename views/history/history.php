
<!-- history -->
<el-row justify="center" type="flex" >


<el-col :span="24" :xs="24" :sm="24" justify="center" type="flex" v-if="historyClass == 'primary'">
    <el-row justify="center" type="flex" v-for="(goodsList, index) in oders">
        <div class="scroll-goodsList">
            <el-col style="min-width: 450px"
                :xs="8"  :sm="8" :md="8" :lg="8" :xl="8" 
                v-for="(row, index_) in goodsList.goods">
                <el-card :body-style="{ padding: '0px', display: 'flex', height: '150px', width: '230px' }">
                    <img  :src="row.icon" class="image-cart">
                    <div style="padding: 14px">
                        <div><b>{{row.good_name}}</b></div>
                        <div class="bottom clearfix nowrap">
                            <br>
                            <span>
                                {{row.price}}.00 $
                            </span>
                            <br>
                            <span>
                                Count: {{row.count}}
                            </span>
                        </div>
                    </div>
                </el-card>
            </el-col>
<br>
        </div>
        <el-col :xs="6"  :sm="6" :md="6" :lg="6" :xl="6" class="center-inner">
            <span>
                <b>Total price: {{goodsList.total_price}} $</b>
            </span>
        </el-col>
    </el-row>
</el-col>


</el-row>
