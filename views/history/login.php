
<!-- login -->
<el-row justify="center" type="flex">
    <el-col :span="12" :xs="24" :sm="8" class="nav user-form" v-if="historyClass == 'primary'">
    <el-row justify="center" type="flex">
        <el-col :xs="24"  :sm="24" :md="24" :lg="24" :xl="24">
            Email: <el-input type="email" 
                @blur="validateEmail"
                placeholder="Please input" 
                v-model="email"></el-input>
        </el-col>
    </el-row>
    <el-row justify="center" type="flex">
        <el-col :xs="24"  :sm="24" :md="24" :lg="24" :xl="24">
            Phone: <el-input type="number"  placeholder="Please input" v-model="phone"></el-input>
        </el-col>
    </el-row>
    <el-row justify="center" type="flex">
        <el-col :xs="24"  :sm="24" :md="24" :lg="24" :xl="24">
            <el-button type="primary" @click="getHistory">Submit</el-button>
        </el-col>
    </el-row>
</el-col>
</el-row>
