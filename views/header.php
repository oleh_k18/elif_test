<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> <?php echo $title; ?></title>

    <!-- <script src="https://unpkg.com/vue@3"></script> -->
    <script src="https://unpkg.com/vue@2"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    
    <link rel="stylesheet" href="https://unpkg.com/element-ui/lib/theme-chalk/index.css">
    <!-- import JavaScript -->
    <script src="https://unpkg.com/element-ui/lib/index.js"></script>
    
    <script src="https://unpkg.com/element-ui/lib/umd/locale/en.js"></script>

    <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>  
    <script defer src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key=AIzaSyDb7q9sizf_dPZvOY-PJNNPOPlFDqMN5q8"></script>

    <script src="https://www.google.com/recaptcha/api.js?render=6LcKp7kgAAAAAMJY6z8a5AJ3lAvxbEsTOJES_PYe"></script>

</head>
<style>
    .header {
        width: 80%;
        margin: auto;
        margin-top: 25px;
        text-align: left;
    }

    body {
        text-align: center;
        min-height: 1000px;
    }

    .el-row {
        padding: 5px;
    }
    .nav {
        border: solid #409EFF;
        border-radius: 5px;
        margin-top: 11px;
        padding: 20px;
    }
    .user-form {
        text-align: left;
    }
    .image {
        max-height: 220px;
    }
    .image-cart {
        max-width: 350px;
    }
    .card-cart {
        /* padding: 0px;
        display: flex;  */
        max-height: 100px;
    }
    #footer {
        bottom: 0px;
        display: block;
        position: fixed;
        text-align: center;
        width: 100%;
        background-color: white;
        padding: 10px;
    }
    .center-inner {
        display: flex;
        justify-content: center;
        align-items: center;
    }
    .scroll-goodsList {
        overflow: auto;
        display: flex;
        width: 900px;
    }
    #map {
        height: 400px;
    }

    .error {
        animation-name: shaking;
        animation-duration: 0.5s;
    }
    .error > input {
        border-color: red;
    }

    @keyframes shaking {
        0%   {left:0px; }
        20%  {left:-5px;}
        40%  {left:5px;}
        60%  {left:-5px;}
        80%  {left:5px;}
        90%  {left:-5px;}
        100% {left:0px;}
    }

    .nowrap {
        white-space: nowrap;
    }

</style>
<body>
<div id="app">

<div class="header">
    <el-button :type="shopClass" @click="changePage('Shop')">Shop</el-button>
    <el-button :type="shopingCartClass" @click="changePage('Shoping cart')">Shoping cart</el-button>
    <el-button :type="historyClass" @click="changePage('history')">History</el-button>
</div>