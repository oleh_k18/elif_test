
<!-- Goods -->
<el-col :span="20" :xs="24" :sm="17" justify="center" type="flex" v-if="shopClass == 'primary'">
    <div style="margin: auto;max-width: 1500px;">
    <el-col :xs="20"  :sm="10" :md="10" :lg="7" :xl="5" v-for="(row, index) in goods" style="margin: 10px;" v-if="shop == 0 || shop == row.shop_id">
        <el-card :body-style="{ padding: '0px' }">
            <img :src="row.icon" class="image">
            <div style="padding: 14px;">
                <span><b>{{row.name}}</b></span>
                <div class="bottom clearfix">
                    <span>
                        {{row.price}}.00 $
                    </span>
                    <br>
                    <br>
                    <el-button type="primary" @click="add2Cart(row.id)" :disabled="shopShopingCart != 0 && shopShopingCart != row.shop_id">add to Cart</el-button>
                </div>
            </div>
        </el-card>
    </el-col>
    </div>
</el-col>

