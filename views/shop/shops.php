
<!-- SHOPS  -->
<el-col :span="4" :xs="24" :sm="6" :offset="1" class="nav" v-if="shopClass == 'primary'">
    <el-row justify="center" type="flex">
        <el-col :xs="24"  :sm="24" :md="24" :lg="24" :xl="24">
            <el-radio v-model="shop" :label="0" border >All shops</el-radio>
        </el-col>
    </el-row>
    <el-row justify="center" type="flex" v-for="(row, index) in shops">
        <el-col :xs="24"  :sm="24" :md="24" :lg="24" :xl="24">
            <el-radio v-model="shop" :label="row.id" border>{{row.name}}</el-radio>
        </el-col>
    </el-row>
</el-col>
