<?php


class ShopsController 
{
    public function actionIndex()
    {
        
        $shopsList = array();
        $shopsList = Shops::getShopsList();

        require_once(ROOT.'/views/shop/index.php');
        return '';
    }

    /**
     * print json array
     */
    public function actionList()
    {

        $shopList = Shops::getShopsList();

        $shopList = json_encode($shopList);

        echo $shopList;
        return $shopList;
    }
    /**
     * print save order to data base
     */
    public function actionSave()
    {
        $res['ok'] = 0;

        $name = $_POST['name'];
        $email = $_POST['email'];
        $phone = $_POST['phone'];
        $address = $_POST['address'];

        $user_id = Users::getUserId($email, $phone);
        if ($user_id == 0) {
            $user_id = Users::createUser($name, $email, $phone, $address);
        }

        $totalPrice = $_POST['totalPrice'];
        $shop_id = $_POST['shop'];

        $order_id = Shops::saveOrder($user_id, $totalPrice);

        $shopingCart = $_POST['shopingCart'];
        $shopingCart = json_decode($shopingCart);
        foreach ($shopingCart as $row) {
            $name = $row->name;
            $good_id = $row->id;
            $count = $row->count;
            $id = Shops::saveShopingCart($user_id, $order_id, $shop_id, $good_id, $count);
            if ($id > 0) $res['ok'] = 1;
        }
        
        $res = json_encode($res);

        echo $res;
        return $res;
    }

    /**
     * return captcha response
     */
    public function actionGoogle()
    {
        $token = $_REQUEST['token'];
        $res = GoogleCaptcha::checkToken($token);
    }
}

