<?php


class HistoryController
{
    public function actionIndex()
    {
        
        return '';
    }

    /**
     * print json array
     */
    public function actionList()
    {
        $email = $_POST['email'];
        $phone = $_POST['phone'];


        // echo '---';

        $user_id = Users::getUserId($email, $phone);

        $ordersList = History::getOrdersByUserId($user_id);

        $result = array();
        if (!empty($ordersList)) {
            foreach ($ordersList as $row) {

                $order_id = $row->id;
                $res = History::getGoodsByOrderId($order_id);

                $arr = array();
                $arr['total_price'] = $row->total_price;
                $arr['order_id'] = $order_id;
                $arr['goods'] = $res;
                
                $result[] = $arr;
            }
        }

        $ordersList = json_encode($result);

        echo $ordersList;
        return $ordersList;
    }
}

