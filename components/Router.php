<?php

class Router 
{
    private $routes;

    public function __construct()
    {
        $routePath = ROOT.'/config/routes.php';
        $this->routes = include($routePath);
        
    }


    /**
     * Returns request string
     */
    public function getURI()
    {
        if (!empty($_SERVER['REQUEST_URI'])) {
            $url = $_SERVER['REQUEST_URI'];
            return trim($url, '/');
        }
    }

    /**
     * run controllers and actions
     */
    public function run()
    {
        $url = $this->getURI();

        foreach ($this->routes as $uriPattern => $path) {
            // echo "<br>uriPattern: ".$uriPattern;
            // echo "<br>path: ".$path;
            // echo "<br>url: ".$url;
            if ($uriPattern != $url) continue;
            $internalRoute = preg_replace("~$uriPattern~", $path, $url);
            $segments = explode('/', $internalRoute);
            // echo '<br>internalRoute: '.$internalRoute;
            // echo '<br>';
            // echo "<pre>";
            // var_dump($segments);
            if (empty($segments[0])) return;
            $controllerName =  array_shift($segments); // get first and remove from array
            $controllerName = $controllerName.'Controller'; // concat
            $controllerName = ucfirst($controllerName); // uppercase first latter

            $actionName = array_shift($segments); // get first and remove from array
            $actionName = ucfirst($actionName); // uppercase first latter
            $actionName = 'action'.$actionName;  // concat
            // echo $actionName;

            // $parameters = $segments; // other is parameters
            $parameters = array();
            $controllerFile = ROOT.'/controllers/'.$controllerName.'.php'; // path to controller

            // if exist file - include
            // echo $controllerFile;
            if (file_exists($controllerFile)) {
                include_once($controllerFile);
            } else {
                exit('Erorr. File doesen\'t exist');
            }


            $controllerObject = new $controllerName; // make new object

            $actionExist = method_exists($controllerObject, $actionName);
            if ($actionExist) {
                $result = call_user_func_array(array($controllerObject, $actionName), $parameters);
                // echo '<pre>';
                // var_dump($actionName);
                // var_dump($controllerName);
                // var_dump($result);
                if ($result != null) {
                    break;
                }
            }


        }
        // echo 'work';
    }
}
